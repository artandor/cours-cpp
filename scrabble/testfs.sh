#!/usr/bin/env bash

BIN_LOCATION="/home/build/myfs-test"
TEST_ITER=0

# Test 1
((TEST_ITER++))
${BIN_LOCATION} -d "/home/scrabble/dico-test.txt" -s "estt"
status=$?
if [ ${status} -ne 0 ] ; then
    echo "Failed test $TEST_ITER"
    exit 1
fi
echo "Passed test $TEST_ITER"

# Test 2
((TEST_ITER++))
${BIN_LOCATION} -d "/tmp/dic.txt" -s "estt" -s "abc"
status=$?
if [ ${status} -ne 0 ] ; then
    echo "Failed test $TEST_ITER"
    exit 1
fi
echo "Passed test $TEST_ITER"

# Test 3
((TEST_ITER++))
${BIN_LOCATION} -d "/tmp/dic.txt" -s "azerazeazeazefqsef" -s "wanted"
status=$?
if [ ${status} -ne 0 ] ; then
    echo "Failed test $TEST_ITER"
    exit 1
fi
echo "Passed test $TEST_ITER"

# Test 4
((TEST_ITER++))
${BIN_LOCATION}
status=$?
if [ ${status} -ne 2 ] ; then
    echo "Failed test $TEST_ITER"
    exit 1
fi
echo "Passed test $TEST_ITER"

# Test 5
((TEST_ITER++))
${BIN_LOCATION} -d "/tmp/dic.txt"
status=$?
if [ ${status} -ne 2 ] ; then
    echo "Failed test $TEST_ITER"
    exit 1
fi
echo "Passed test $TEST_ITER"

# Test 6
((TEST_ITER++))
${BIN_LOCATION} -d "/tmp/dic.txt" -saa -s "azeazrfsdaza"
status=$?
if [ ${status} -ne 1 ] ; then
    echo "Failed test $TEST_ITER"
    exit 1
fi
echo "Passed test $TEST_ITER"

# Test 7
((TEST_ITER++))
${BIN_LOCATION} -d "/shitname/mysuperdict.txt" -saa -s "azeazrfsdaza"
status=$?
if [ ${status} -ne 3 ] ; then
    echo "Failed test $TEST_ITER"
    exit 1
fi
echo "Passed test $TEST_ITER"

# Tests PROF
echo "START TEST PROF"

#Test 8
((TEST_ITER++))
${BIN_LOCATION} -d "/home/scrabble/mysampledict.txt"
status=$?
if [ ${status} -ne 2 ] ; then
    echo "Failed test $TEST_ITER"
    exit 1
fi
echo "Passed test $TEST_ITER"

#Test 9
((TEST_ITER++))
${BIN_LOCATION} -d "/home/scrabble/mysampledict.txt" "plop"
status=$?
if [ ${status} -ne 2 ] ; then
    echo "Failed test $TEST_ITER"
    exit 1
fi
echo "Passed test $TEST_ITER"

#Test 10
((TEST_ITER++))
${BIN_LOCATION} d "/home/scrabble/mysampledict.txt" -s -s "plop"
status=$?
if [ ${status} -ne 2 ] ; then
    echo "Failed test $TEST_ITER"
    exit 1
fi
echo "Passed test $TEST_ITER"

#Test 11
((TEST_ITER++))
${BIN_LOCATION} -s "plop"
status=$?
if [ ${status} -ne 2 ] ; then
    echo "Failed test $TEST_ITER"
    exit 1
fi
echo "Passed test $TEST_ITER"

#Test 12
((TEST_ITER++))
${BIN_LOCATION} -d "/home/scrabble/mysampledict.txt" -s "wzt"
status=$?
if [ ${status} -ne 1 ] ; then
    echo "Failed test $TEST_ITER"
    exit 1
fi
echo "Passed test $TEST_ITER"

#Test 13
((TEST_ITER++))
${BIN_LOCATION} -d "/home/scrabble/mysampledict.txt" -s "wzt" -s "A"
status=$?
if [ ${status} -ne 0 ] ; then
    echo "Failed test $TEST_ITER"
    exit 1
fi
echo "Passed test $TEST_ITER"