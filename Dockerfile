FROM debian:stretch

RUN apt-get update \
&& apt-get install gcc g++ valgrind wget -y

ADD ./ /home

VOLUME /home/build /home/build
