#include <iostream>
#include <assert.h>
#include "mychainedstack.h"
#include "mystack.h"

using namespace std;

// Main de test simple
int main()
{
	// Banner
 	cout << "Mychainedstack - test" << endl;

	// Instanciation de la classe dérivée
	Mychainedstack _cs;

	// Reference à la baseclass (polymorphisme)
    Mystack &cs = _cs;

    assert(cs.pop() == -16384);
	
	// Push
    cs.push(0);
	cs.push(1);
	cs.push(2);
	cs.push(3);
    cs.push(4);
    cs.push(5);
    cs.push(6);

	// Pop
    assert(cs.pop() == 6);
    assert(cs.pop() == 5);

	// Opérateur %
    assert(cs % 2 == 1);
    assert(cs % 1 == -65536);
    assert(cs % -121 == -65536);

	// Clear
	cs.clear();
    assert(cs.pop() == -16384);
    cs.push(12);
    cs.push(18);
    assert(cs.pop() == 18);

  	return 0;
}
